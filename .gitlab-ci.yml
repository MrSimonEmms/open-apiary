include:
  - project: "mrsimonemms/gitlab-ci-tasks"
    ref: "master"
    file: "tasks/common.yml"

image: node:12-alpine

.e2e-test:
  stage: test
  variables:
    DB_LOGGING: "true"
    DB_MIGRATIONS_RUN: "true"
    DB_SYNC: "false"
    IS_NUXT_DISABLED: "true"
    JWT_SECRET: jwt-secret-token
    UPLOAD_PATH: ${PWD}/uploads
  script:
    - rm -f ${DB_NAME}
    - npm run typeorm -- migration:run
    - LOG_LEVEL=silent npm run test:server:e2e

###########
# Install #
###########
install:
  stage: install
  cache:
    paths:
      - ./node_modules
      - ./data/node_modules
  artifacts:
    paths:
      - ./node_modules
      - ./data/node_modules
  script:
    - apk add --no-cache g++ git make python
    - npm ci --unsafe-perm # Required for the patch-package postinstall task
    - (cd data && npm ci) # Used for ingesting data

########
# Test #
########
audit:
  stage: test
  allow_failure: true
  script: npm audit

e2e-mariadb:
  extends: .e2e-test
  services:
    - mariadb
  variables:
    DB_TYPE: mariadb
    DB_HOST: mariadb
    DB_USER: apiary
    DB_PASS: db-password
    DB_NAME: apiary
    DB_PORT: 3306
    MYSQL_DATABASE: apiary
    MYSQL_PASSWORD: db-password
    MYSQL_RANDOM_ROOT_PASSWORD: "yes"
    MYSQL_USER: apiary

e2e-mysql:
  extends: .e2e-test
  services:
    - mysql
  variables:
    DB_TYPE: mysql
    DB_HOST: mysql
    DB_USER: apiary
    DB_PASS: db-password
    DB_NAME: apiary
    DB_PORT: 3306
    MYSQL_DATABASE: apiary
    MYSQL_PASSWORD: db-password
    MYSQL_RANDOM_ROOT_PASSWORD: "yes"
    MYSQL_USER: apiary

e2e-sqlite:
  extends: .e2e-test
  variables:
    DB_NAME: ./db.sql
    DB_TYPE: sqlite

lint:
  stage: test
  script: npm run lint

unit:
  stage: test
  script: npm test

#############
# Pre-build #
#############
build_artifact:
  stage: pre_build
  artifacts:
    paths:
      - ./dist
  script:
    - APP_NAME=${CI_PROJECT_NAME}
    - export BUILD_ID="${CI_COMMIT_SHA}"
    - |
      if [ -s VERSION ]; then
        echo "Setting VERSION as VERSION file"
        export VERSION="$(cat VERSION)"
        npm version "${VERSION}" --no-git-tag-version
      else
        echo "Setting VERSION as branch name"
        export VERSION="${CI_COMMIT_REF_SLUG}"
      fi
      echo $VERSION
    - npm run build

#########
# Build #
#########
docker_build_amd64:
  extends: .docker_build

docker_build_arm32v6:
  extends: .docker_build
  only:
    refs:
      - master
      - develop
      - tags
  variables:
    CIT_DOCKER_ARCH: arm32v6
    CIT_DOCKER_ARGS_IMG: arm32v6/alpine

docker_build_arm32v7:
  extends: .docker_build
  only:
    refs:
      - master
      - develop
      - tags
  variables:
    CIT_DOCKER_ARCH: arm32v7
    CIT_DOCKER_ARGS_IMG: arm32v7/alpine

docker_build_arm64v8:
  extends: .docker_build
  only:
    refs:
      - master
      - develop
      - tags
  variables:
    CIT_DOCKER_ARCH: arm64v8
    CIT_DOCKER_ARGS_IMG: arm64v8/alpine

###########
# Publish #
###########
docker_publish_latest:
  extends: .docker_publish
  only:
    refs:
      - master
      - tags
  variables:
    CIT_DOCKER_ARCH_LIST: amd64,arm32v6,arm32v7,arm64v8
    CIT_DOCKER_TAG: latest
    CIT_PUBLISH_VERSIONED_TAG: 1

docker_publish_develop:
  extends: .docker_publish
  only:
    refs:
      - develop
  variables:
    CIT_DOCKER_ARCH_LIST: amd64,arm32v6,arm32v7,arm64v8
    CIT_DOCKER_TAG: ${CI_COMMIT_REF_SLUG}

docker_publish_feature:
  extends: .docker_publish
  except:
    refs:
      - master
      - tags
  variables:
    CIT_DOCKER_TAG: ${CI_COMMIT_REF_SLUG}

###############
# Pre-release #
###############
pages:
  stage: pre_release
  script:
    - npm i -g vuepress # Install as global as npm breaks the file tree
    - npm run docs:build
  variables:
    CONFIG_BASE: /open-apiary/
  artifacts:
    paths:
      - public
  except:
    variables:
      - $CIT_NO_RELEASE == "1"
  only:
    refs:
      - master

###########
# Release #
###########
update_version:
  extends: .repo_update

github_release:
  stage: release
  variables:
    GITHUB_USER: mrsimonemms
    GITHUB_REPO: open-apiary
  script:
    - apk add --no-cache curl jq zip
    - sh ./scripts/release.sh
  except:
    variables:
      - $CIT_NO_RELEASE == "1"
  only:
    refs:
      - master
